import pygame
import sys
import os
import random

# Classes
class Spaceship(pygame.sprite.Sprite):
    def __init__(self, screen_width, screen_height, offset):
        super().__init__()
        self.offset = offset
        self.screen_width = screen_width
        self.screen_height = screen_height
        self.image = load_image("generics/spaceship.png")
        self.rect = self.image.get_rect(midbottom = ((self.screen_width + self.offset)/2, self.screen_height + 20))
        self.speed = 6
        self.laser_group = pygame.sprite.Group()
        self.laser_ready = True
        self.laser_time = 0
        self.laser_delay = 300
        self.laser_sound = load_sfx("sounds/laser.ogg")

    def get_user_input(self):
        keys = pygame.key.get_pressed()
        if keys[pygame.K_RIGHT]:
            self.rect.x += self.speed
        if keys[pygame.K_LEFT]:
            self.rect.x -= self.speed
        if keys[pygame.K_SPACE] and self.laser_ready:
             self.laser_ready = False
             laser = Laser(self.rect.center, 5, self.screen_height)
             self.laser_group.add(laser)
             self.laser_time = pygame.time.get_ticks()
             self.laser_sound.play()

    def update(self):
        self.get_user_input()
        self.constrain_movement()
        self.laser_group.update()
        self.recharge_laser()

    def constrain_movement(self):
        if self.rect.right > self.screen_width:
            self.rect.right = self.screen_width
        if self.rect.left < self.offset:
            self.rect.left = self.offset

    def recharge_laser(self):
         if not self.laser_ready:
              current_time = pygame.time.get_ticks()
              if current_time - self.laser_time >= self.laser_delay:
                   self.laser_ready = True

    def reset(self):
        self.rect = self.image.get_rect(midbottom = ((self.screen_width + self.offset)/2, self.screen_height + 20))
        self.laser_group.empty()

class Laser(pygame.sprite.Sprite):
    def __init__(self, position, speed, screen_height):
        super().__init__()
        self.image = pygame.Surface((4, 15))
        self.image.fill((255, 255, 255))
        adjusted_position = (position[0], position[1] - 25)
        self.rect = self.image.get_rect(center = adjusted_position)
        self.speed = speed
        self.screen_height = screen_height

    def update(self):
        self.rect.y -= self.speed
        if self.rect.y > self.screen_height + 15 or self.rect.y < 0:
            self.kill()

class Block(pygame.sprite.Sprite):
	def __init__(self, x, y):
		super().__init__()
		self.image = pygame.Surface((3, 3))
		self.image.fill((255, 255, 255))
		self.rect = self.image.get_rect(topleft = (x, y))

grid = [
[0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0],
[0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0],
[0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0],
[0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0],
[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
[1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1],
[1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1],
[1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1]]

class Obstacle:
	def __init__(self, x, y):
		self.blocks_group = pygame.sprite.Group()
		for row in range(len(grid)):
			for column in range(len(grid[0])):
				if grid[row][column] == 1:
					pos_x = x + column * 3
					pos_y = y + row * 3
					block = Block(pos_x, pos_y)
					self.blocks_group.add(block)

class Game:
    def __init__(self, screen_width, screen_height, offset):
        self.screen_width = screen_width
        self.screen_height = screen_height
        self.offset = offset
        self.level = 1
        self.update_level_surface()
        self.spaceship_group = pygame.sprite.GroupSingle()
        self.spaceship_group.add(Spaceship(self.screen_width, self.screen_height, self.offset))
        self.obstacles = self.create_obstacles()
        self.alien_group = pygame.sprite.Group()
        self.create_aliens()
        self.aliens_direction = 1
        self.alien_lasers_group = pygame.sprite.Group()
        self.mystery_ship_group = pygame.sprite.GroupSingle()
        self.lives = 3
        self.run = True
        self.score = 0
        self.highscore = 0
        self.highscore = load_highscore()
        load_sfx("sounds/music.ogg").play(-1)
        self.explosion_sound = load_sfx("sounds/explosion.ogg")
        
    def create_obstacles(self):
        obstacle_width = len(grid[0]) * 3
        gap = (self.screen_width + self.offset - (4 * obstacle_width)) / 5
        obstacles = []
        for i in range(4):
            offset_x = (i + 1) * gap + i * obstacle_width
            obstacle = Obstacle(offset_x, self.screen_height - 100)
            obstacles.append(obstacle)
        return obstacles
    
    def create_aliens(self):
        for row in range(5):
            for column in range (11):
                x = 75 + column * 55
                y = 110 + row * 55

                if row == 0:
                    alien_type = 3
                elif row in (1, 2):
                    alien_type = 2
                else:
                    alien_type = 1

                alien = Alien(alien_type, x + self.offset / 2, y)
                self.alien_group.add(alien)
    
    def move_aliens(self):
        self.alien_group.update(self.aliens_direction)
        alien_sprites = self.alien_group.sprites()
        for alien in alien_sprites:
            if alien.rect.right >= self.screen_width + self.offset / 2:
                self.aliens_direction = -1
                self.alien_move_down(2)
            elif alien.rect.left <= self.offset / 2:
                self.aliens_direction = 1
                self.alien_move_down(2)

    def alien_move_down(self, distance):
        if self.alien_group:
            for alien in self.alien_group.sprites():
                alien.rect.y += distance

    def alien_shoot_laser(self):
        if self.alien_group.sprites():
            random_alien = random.choice(self.alien_group.sprites())
            laser_sprite = Laser(random_alien.rect.center, -6, self.screen_height)
            self.alien_lasers_group.add(laser_sprite)

    def create_mystery_ship(self):
        self.mystery_ship_group.add(MysteryShip(self.screen_width, self.offset))

    def check_for_collissions(self):
        if self.spaceship_group.sprite.laser_group:
            for laser_sprite in self.spaceship_group.sprite.laser_group:
                aliens_hit = pygame.sprite.spritecollide(laser_sprite, self.alien_group, True)
                if aliens_hit:
                    self.explosion_sound.play()
                    for alien in aliens_hit:
                        self.score += alien.type * 100
                        self.check_for_highscore()
                        laser_sprite.kill()
                if pygame.sprite.spritecollide(laser_sprite, self.mystery_ship_group, True):
                    self.score += 500
                    self.explosion_sound.play()
                    self.check_for_highscore()
                    laser_sprite.kill()

                for obstacle in self.obstacles:
                    if pygame.sprite.spritecollide(laser_sprite, obstacle.blocks_group, True):
                        laser_sprite.kill()
        
        if self.alien_lasers_group:
            for laser_sprite in self.alien_lasers_group:
                if pygame.sprite.spritecollide(laser_sprite, self.spaceship_group, False):
                    laser_sprite.kill()
                    self.lives -= 1
                    if self.lives == 0:
                        self.game_over()

                for obstacle in self.obstacles:
                    if pygame.sprite.spritecollide(laser_sprite, obstacle.blocks_group, True):
                        laser_sprite.kill()

        if self.alien_group:
            for alien in self.alien_group:
                for obstacle in self.obstacles:
                    pygame.sprite.spritecollide(alien, obstacle.blocks_group, True)

                if pygame.sprite.spritecollide(alien, self.spaceship_group, False):
                    self.game_over()

    def game_over(self):
        self.run = False

    def reset(self):
        self.level = 1
        self.run = True
        self.lives = 3
        self.spaceship_group.sprite.reset()
        self.alien_group.empty()
        self.alien_lasers_group.empty()
        self.create_aliens()
        self.mystery_ship_group.empty()
        self.obstacles = self.create_obstacles()
        self.score = 0
        self.update_level_surface()
        
    def check_for_highscore(self):
        if self.score > self.highscore:
            self.highscore = self.score

            save_highscore(self.highscore)

    def check_level_complete(self):
        if not self.alien_group:
            return True
        return False

    def next_level(self):
        self.level += 1
        self.alien_group.empty()
        self.create_aliens()
        self.aliens_direction = 1
        self.obstacles = self.create_obstacles()
        self.run = True
        self.update_level_surface()

    def update_level_surface(self):
        level_text = f"LEVEL {self.level:02}"
        self.level_surface = font.render(level_text, False, (255, 255, 255))

class Alien(pygame.sprite.Sprite):
    def __init__(self, type, x, y):
        super().__init__()
        self.type = type
        self.image = load_image(f"aliens/alien_{type}.png")
        self.rect = self.image.get_rect(topleft = (x, y))

    def update(self, direction):
        self.rect.x += direction

class MysteryShip(pygame.sprite.Sprite):
    def __init__(self, screen_width, offset):
        super().__init__()
        self.screen_width = screen_width
        self.offset = offset
        self.image = load_image("aliens/mystery.png")

        x = random.choice([self.offset, self.screen_width + self.offset - self.image.get_width()])
        if x == self.offset:
            self.speed = 3
        else:
            self.speed = -3

        self.rect = self.image.get_rect(topleft = (x, 90))
    
    def update(self):
        self.rect.x += self.speed
        if self.rect.right > self.screen_width + self.offset:
            self.kill()
        elif self.rect.left < self.offset:
            self.kill()

# Functions
def load_image(filename):
    internal_path = os.path.join("_internal", filename)
    src_path = os.path.join("src", filename)
    if os.path.exists(internal_path):
        return pygame.image.load(internal_path)
    elif os.path.exists(src_path):
        return pygame.image.load(src_path)

# Load sounds
def load_sfx(filename):
    internal_path = os.path.join("_internal", filename)
    src_path = os.path.join("src", filename)
    if os.path.exists(internal_path):
        return pygame.mixer.Sound(internal_path)
    elif os.path.exists(src_path):
        return pygame.mixer.Sound(src_path)
    
# Load custom font
if os.path.exists(os.path.join("_internal", "fonts", "space_invaders.ttf")):
    font_path = os.path.join("_internal", "fonts", "space_invaders.ttf")
elif os.path.exists(os.path.join("fonts", "space_invaders.ttf")):
    font_path = os.path.join("fonts", "space_invaders.ttf")

# AppData folder
def get_appdata_folder():
    return os.path.join(os.getenv("AppData", "Roaming"), "SpaceInvaders")

# Load the high_score
def load_highscore():
    high_score_file = os.path.join(get_appdata_folder(), "high_score.dat")
    if os.path.exists(high_score_file):
        with open(high_score_file, "r") as file:
            data = file.read()
            score = int(data.split("=")[1]) if "=" in data else 0
            return score
    else:
        return 0
    
# Save the high_score
def save_highscore(score):
    app_data_folder = os.path.join(get_appdata_folder())
    os.makedirs(app_data_folder, exist_ok=True)
    high_score_file = os.path.join(app_data_folder, "high_score.dat")
    with open(high_score_file, "w") as file:
        file.write(f"high_score={score}")

# Init
pygame.init()

# Global variables
SCREEN_WIDTH = 750
SCREEN_HEIGHT = 700
OFFSET = 50
PAUSED = False

# Icon
icon_size = (32, 32)
icon_path = load_image("icons/icon.png")
icon_image = pygame.transform.scale(icon_path, icon_size)
pygame.display.set_icon(icon_image)

# Colors
GREY = (29, 29, 27)
YELLOW = (243, 216, 63)

# Font text
font = pygame.font.Font(font_path, 20)
game_over_surface = font.render("GAME OVER", False, (255, 255, 255))
score_text_surface = font.render("SCORE", False, (255, 255, 255))
highscore_text_surface = font.render("HIGH-SCORE", False, (255, 255, 255))

# Window variables
screen = pygame.display.set_mode((SCREEN_WIDTH + OFFSET, SCREEN_HEIGHT + 2 * OFFSET))
pygame.display.set_caption("Space Invaders")

# Clock
clock = pygame.time.Clock()

# Game
game = Game(SCREEN_WIDTH, SCREEN_HEIGHT, OFFSET)

# Shoot laser
SHOOT_LASER = pygame.USEREVENT
pygame.time.set_timer(SHOOT_LASER, 300)

# Timer
MYSTERSHIP = pygame.USEREVENT + 1
pygame.time.set_timer(MYSTERSHIP, random.randint(4000, 8000))

# Main loop
while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        if event.type == SHOOT_LASER and game.run:
            game.alien_shoot_laser()
        if event.type == MYSTERSHIP and game.run:
            game.create_mystery_ship()
            pygame.time.set_timer(MYSTERSHIP, random.randint(4000, 8000))
        
        keys = pygame.key.get_pressed()
        if keys[pygame.K_SPACE] and game.run == False:
            game.reset()

        if event.type == pygame.KEYDOWN and event.key == pygame.K_p:
            PAUSED = not PAUSED

    if PAUSED:
        pause_font = pygame.font.Font(font_path, 32)
        pause_surface = pause_font.render("PAUSED", True, (255, 255, 255))
        pause_rect = pause_surface.get_rect(center=((game.screen_width + game.offset) / 2, game.screen_height / 2))
        screen.blit(pause_surface, pause_rect)
        pygame.display.flip()
        continue
    
    if game.run:
        game.spaceship_group.update()
        game.move_aliens()
        game.alien_lasers_group.update()
        game.mystery_ship_group.update()
        game.check_for_collissions()

        if game.check_level_complete():
            game.next_level()

    screen.fill(GREY)
    pygame.draw.rect(screen, YELLOW, (10, 10, 780, 780), 2, 0, 60, 60, 60, 60)
    pygame.draw.line(screen, YELLOW, (25, 730), (775, 730), 3)
    if game.run:
        screen.blit(game.level_surface, (640, 750, 50, 50))
    else:
        screen.blit(game_over_surface, (620, 750, 50, 50))
    x = 50
    for life in range(game.lives):
        screen.blit(game.spaceship_group.sprite.image, (x, 735))
        x += 50
    screen.blit(score_text_surface, (55, 20, 50, 50))
    formated_score = str(game.score).zfill(5)
    score_surface = font.render(formated_score, False, (255, 255, 255))
    screen.blit(score_surface, (55, 45, 50, 50))
    screen.blit(highscore_text_surface, (600, 20, 50, 50))
    formatted_highscore = str(game.highscore).zfill(5)
    highscore_surface = font.render(formatted_highscore, False, (255, 255, 255))
    screen.blit(highscore_surface, (670, 40, 50, 50))
    game.spaceship_group.draw(screen)
    game.spaceship_group.sprite.laser_group.draw(screen)
    for obstacle in game.obstacles:
         obstacle.blocks_group.draw(screen)
    game.alien_group.draw(screen)
    game.alien_lasers_group.draw(screen)
    game.mystery_ship_group.draw(screen)

    pygame.display.update()
    clock.tick(60)